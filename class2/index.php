<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
  <!-- Custom Fonts -->
  <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
  <!--favicon.ico  -->
  <link rel="icon" href="favicon.ico">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="css/responsive.css">
  <!-- modernizr-2.8.3.min.js  -->
  <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
   <?php 
    $var=3;
    if(empty($var)){
   	  echo "variable is empty";
    }
    else{
    	echo "variable is $var";
    }
    ?>

  <?php 
  $var = array(1,1.,null,"razib");
  foreach ($var as $value) {
  	echo gettype($value),"\n";
  }
  ?>
  <?php
  $var=array("this","is","array");
 	 echo is_array($var)?"array":"not an array";
  $var1="this is not an array";
  	echo is_array($var1)?"array":"not array";
  ?>
  <?php
  $var="habib";
  echo is_int($var)?"integer":"not integer";
  $var=10;
  echo is_int($var)?"integer":"not integer";
  $var=null;
  echo is_null($var)?"null":"not null";
  $var="rahat";
  echo is_string("$var")?"string":"not string";
  $var=array("a"=>"razib","b"=>"nujhat","c"=>array("habib","rasel","amzad"));
  echo print_r($var);
  $var="ccc";
  if(isset($var)){
  	echo "this is a set so i will print";
  }
  $var = array(1, 2, array("a", "b", "c"));
var_dump($var);
  ?>


  <!-- jQuery -->
  <script src="js/jquery.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
  <!-- Bootstrap JavaScript -->
  <script src="js/bootstrap.min.js"></script>
  <!-- Plugin JavaScript -->
  <script src="js/plugins.js"></script>
  <!-- Custom Theme JavaScript -->
  <script src="js/main.js"></script>
</body>
</html>
